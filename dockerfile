FROM python:3.12.0a1-alpine3.16
RUN apk --no-cache add git
RUN git clone https://github.com/tornadoweb/tornado.git
WORKDIR /tornado
COPY ./tornado/helloworld.py /tornado/helloworld.py
CMD ["python3", "helloworld.py"]
EXPOSE 8888